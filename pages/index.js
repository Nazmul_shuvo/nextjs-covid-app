import { BASE_URL } from "../config/url";
import { COUNTRY_LIST_TABLE_ROWS } from "../config/country";
import Table from "../components/Table";

import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";

const Home = ({ data }) => {
  const countries = data ? data.Countries : [];

  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Typography variant="h3" sx={{ py: 2 }}>
        Covid situation of countries at a glance
      </Typography>
      <Paper elevation={3}>
        <Table
          rows={countries}
          heads={COUNTRY_LIST_TABLE_ROWS}
          hoverEffect={true}
        />
      </Paper>
    </Box>
  );
};

export default Home;

export const getStaticProps = async () => {
  // fetching list of the countries with Covid data
  const res = await fetch(`${BASE_URL}/summary`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};
