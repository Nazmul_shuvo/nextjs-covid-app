import { useState } from "react";
import { useRouter } from "next/router";
import { BASE_URL } from "../../config/url";
import Table from "../../components/Table";
import { COUNTRY_DETAILS_TABLE_ROWS } from "../../config/country";
import { localToUtc } from "../../utils/time";

import {
  Grid,
  TextField,
  Typography,
  Paper,
  Box,
  Button,
  CircularProgress,
} from "@mui/material";

const CountryDetails = ({ country }) => {
  const [currentData, setCurrentData] = useState(country);
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");

  const router = useRouter();
  const { slug } = router.query;

  const handleDateApply = async () => {
    // For time shortage skipping the input check

    // converting text time inputs to UTC, exactly same way the API understands
    let fromUtc = localToUtc(from);
    let toUtc = localToUtc(to);

    // setting data to empty to start the loader
    setCurrentData(null);

    // fetching data depending on time input
    const res = await fetch(
      `${BASE_URL}/country/${slug}?from=${fromUtc}&to=${toUtc}`
    );
    const country = await res.json();

    // setting new data as current active data to show on the table
    setCurrentData(country);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Typography variant="h3" sx={{ py: 2 }}>
        {country[0].Country}
      </Typography>
      <Grid
        container
        justifyContent={"center"}
        alignItems={"center"}
        spacing={2}
        sx={{ py: 2 }}
      >
        <Grid item>
          <TextField
            id="from"
            label="From Date"
            variant="outlined"
            value={from}
            helperText={"'month/date/year' this format only"}
            placeholder={"06/12/2022"}
            onChange={(e) => {
              e.preventDefault();
              setFrom(e.target.value);
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            id="to"
            label="To Date"
            variant="outlined"
            value={to}
            helperText={"'month/date/year' this format only"}
            placeholder={"06/12/2022"}
            onChange={(e) => {
              e.preventDefault();
              setTo(e.target.value);
            }}
          />
        </Grid>
        <Grid item>
          <Button
            color="primary"
            variant="outlined"
            aria-label="add"
            onClick={handleDateApply}
          >
            Apply
          </Button>
        </Grid>
      </Grid>
      {currentData && currentData.length ? (
        <Paper elevation={3}>
          <Table rows={currentData} heads={COUNTRY_DETAILS_TABLE_ROWS} />
        </Paper>
      ) : (
        <CircularProgress />
      )}
    </Box>
  );
};

export default CountryDetails;

// using (getStaticProps and getStaticPaths) functions are better.
// but choosing getServerSideProps over them for API structure.
export const getServerSideProps = async (context) => {
  // fetching country-wise data with default time
  const res = await fetch(`${BASE_URL}/dayone/country/${context.params.slug}`);
  const country = await res.json();

  return {
    props: {
      country,
    },
  };
};
