/**
 * 'name' is to show on the table;
 * 'accessors' are taken exactly same from the API to get value from the object;
 * 'align' is optional to align a column on the table;
 * 'minWidth' is optional to set minimum width of a column;
 */
export const COUNTRY_LIST_TABLE_ROWS = [
  { name: "Country", accessor: "Country", align: "left", minWidth: "250px" },
  { name: "New Confirmed", accessor: "NewConfirmed" },
  { name: "Total Confirmed", accessor: "TotalConfirmed" },
  { name: "New Recovered", accessor: "NewRecovered" },
  { name: "Total Recovered", accessor: "TotalRecovered" },
  { name: "New Deaths", accessor: "TotalRecovered" },
  { name: "Total Deaths", accessor: "TotalDeaths" },
  { name: "Date", accessor: "Date", isTime: true },
];

export const COUNTRY_DETAILS_TABLE_ROWS = [
  { name: "Date", accessor: "Date", align: "left", isTime: true },
  { name: "Confirmed", accessor: "Confirmed" },
  { name: "Recovered", accessor: "Recovered" },
  { name: "Deaths", accessor: "Deaths" },
];
