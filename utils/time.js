export const utcToLocal = (utc) => {
  // 2020-02-24T00:00:00Z to 2/24/2020 6:00:00 AM (BDT)

  let local = new Date(utc);
  let localTime = local.toLocaleTimeString();
  let localDate = local.toLocaleDateString();

  return localDate + " " + localTime;
};

export const localToUtc = (local) => {
  // 12/23/2019 to Mon, 23 Dec 2019 00:00:00 GMT
  let convertedLocal = local.split("/");
  let utc = new Date(
    Date.UTC(convertedLocal[2], convertedLocal[0] - 1, convertedLocal[1])
  ).toUTCString();

  return utc;
};
