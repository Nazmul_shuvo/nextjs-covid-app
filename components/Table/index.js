import React from "react";
import { useRouter } from "next/router";

import Table from "@mui/material/Table";
import TablePagination from "@mui/material/TablePagination";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

import { utcToLocal } from "../../utils/time";

export default function CustomTable({ rows, heads, hoverEffect = false }) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const router = useRouter();

  // handle when row from the table is clicked
  const handleRowClick = (e, href) => {
    e.preventDefault();
    router.push(href);
  };

  // handle row per page update in the footer of the table
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // handle click on next page
  const handleChangePage = (event, newPage) => {
    event.preventDefault();
    setPage(newPage);
  };

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead sx={{ bgcolor: "#E0E0E0" }}>
          <TableRow>
            {heads.map((item, i) => {
              return (
                <TableCell
                  key={i}
                  align={item.align ? item.align : "center"}
                  sx={{
                    minWidth: item.minWidth ? item.minWidth : "auto",
                    fontWeight: "600",
                  }}
                >
                  {item.name}
                </TableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage) // croping the data to fit per page on the table
            .map((row) => {
              return (
                <React.Fragment key={row.ID}>
                  <TableRow
                    onClick={
                      row.Slug
                        ? (event) => handleRowClick(event, row.Slug)
                        : (_) => {}
                    }
                    hover={hoverEffect}
                    sx={{
                      "&:last-child td, &:last-child th": { border: 0 },
                      "&:hover": { cursor: row.Slug ? "pointer" : "default" },
                    }}
                  >
                    {heads.map((item, i) => {
                      return (
                        <TableCell
                          key={i}
                          component="th"
                          scope="row"
                          align={item.align ? item.align : "center"}
                          sx={{
                            minWidth: item.minWidth ? item.minWidth : "auto",
                          }}
                        >
                          {item.isTime
                            ? utcToLocal(row[item.accessor])
                            : row[item.accessor]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                </React.Fragment>
              );
            })}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
  );
}
